//
//  ViewController.h
//  Frasecitas
//
//  Created by manu on 01/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) NSArray *myQuotes;
@property (strong, nonatomic) NSMutableArray *movieQuotes;
@property (nonatomic, strong) IBOutlet UITextView *quoteText;
@property (nonatomic, strong) IBOutlet UISegmentedControl *quoteOptions;

- (IBAction)quoteButtonTapped:(id)sender;
@end
