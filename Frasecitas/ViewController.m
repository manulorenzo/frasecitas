//
//  ViewController.m
//  Frasecitas
//
//  Created by manu on 01/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *pListCatPath = [[NSBundle mainBundle] pathForResource:@"quotes" ofType:@"plist"];
    self.movieQuotes = [NSMutableArray arrayWithContentsOfFile:pListCatPath];
    [self quoteButtonTapped:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)quoteButtonTapped:(id)sender
{
    if (self.quoteOptions.selectedSegmentIndex == 2)
    {
        // 1 - Get the number of rows in the array
        int array_tot = [self.movieQuotes count];
        
        // 2 - Get random index
        int index = (arc4random() % array_tot);
        
        // 3 - Get the quote string for the index
        //    NSString *myQuote = self.myQuotes[index];
        NSString *myQuote = self.movieQuotes[index][@"quote"];
        
        // 4 - Display the quote in the textview
        self.quoteText.text = [NSString stringWithFormat:@"Quotes:\n\n%@", myQuote];
    } else {
        // 2.1 Determine category
        NSString *selectedCategory = @"classic";
        if (self.quoteOptions.selectedSegmentIndex == 1)
        {
            selectedCategory = @"modern";
        }
        
        // 2.2 - filter array by category using predicate
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@", selectedCategory];
        NSArray *filteredArray = [self.movieQuotes filteredArrayUsingPredicate:predicate];
        
        // 2.3 - Get total number in filtered array
        int array_tot = [filteredArray count];
        // 2.4 - safeguard
        if (array_tot >0)
        {
            // 2.5 - Get random index
            int index = (arc4random() % array_tot);
            // 2.6 - Get the quote string for the index
            NSString *quote = filteredArray[index][@"quote"];
            // 2.7 - Check if there's a source
            NSString *source = [[filteredArray objectAtIndex:index] valueForKey:@"source"];
            if (![source length] == 0) {
                quote = [NSString stringWithFormat:@"%@\n\n(%@)", quote, source];
            }
            // 2.8 - Set display string
            if ([selectedCategory isEqualToString:@"classic"])
            {
                quote = [NSString stringWithFormat:@"From classic movie:\n\n%@", quote];
            } else {
                quote = [NSString stringWithFormat:@"Movie quote:\n\n%@", quote];
            }
            self.quoteText.text = quote;
        } else {
            self.quoteText.text = [NSString stringWithFormat:@"No quotes to display"];
        }
    }
}

@end
