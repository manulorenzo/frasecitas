//
//  AppDelegate.h
//  Frasecitas
//
//  Created by manu on 01/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
